package dk.fitfit.triangle.cmd;

import dk.fitfit.triangle.business.Triangle;
import dk.fitfit.triangle.business.TriangleType;


public class Main {
	public static void main(String[] args) {
		if(args.length < 1) {
			System.out.println("Arguments... ?");
			return;
		}

		// TODO: catch NumberFormatException
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);
		int c = Integer.parseInt(args[2]);

		Triangle triangle = new Triangle();
		TriangleType type = triangle.getType(a, b, c);
		if (type == TriangleType.ERROR) {
			System.err.println("The sides given does not form a valid triangle.");
		} else {
			System.out.println("The sides given as input forms a triangle of the type: " + type);
		}
	}
}
