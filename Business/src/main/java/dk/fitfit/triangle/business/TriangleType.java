package dk.fitfit.triangle.business;


public enum TriangleType {
	ISOSCELES,
	EQUILATERAL,
	SCALENE,
	ERROR
}
