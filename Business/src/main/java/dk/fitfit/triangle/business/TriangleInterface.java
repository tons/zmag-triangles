package dk.fitfit.triangle.business;


public interface TriangleInterface {
	TriangleType getType(int a, int b, int c);
}
