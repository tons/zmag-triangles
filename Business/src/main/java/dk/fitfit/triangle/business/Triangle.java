package dk.fitfit.triangle.business;


// Ref.: https://www.mathsisfun.com/triangle.html
public class Triangle implements TriangleInterface {
	@Override
	public TriangleType getType(int a, int b, int c) {
		if (!isPositive(a, b, c) || shortSides(a, b, c)) {
			return TriangleType.ERROR;
		} else if (a == b && b == c) {
			return TriangleType.EQUILATERAL;
		} else if (a == b || b == c || c == a) {
			return TriangleType.ISOSCELES;
		} else {
			return TriangleType.SCALENE;
		}
	}

	private boolean shortSides(int a, int b, int c) {
		return a + b < c || a + c < b || b + c < a;
	}

	private boolean isPositive(int a, int b, int c) {
		return a > 0 && b > 0 && c > 0;
	}
}
