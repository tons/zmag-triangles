package dk.fitfit.triangle.business;

import org.junit.Test;

import static dk.fitfit.triangle.business.TriangleType.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;


public class TriangleTest {

	@Test
	public void testErrorNotBiggerThanZero() throws Exception {
		// Given
		TriangleInterface triangle = new Triangle();
		int a = 0, b = 0, c = 0;

		// When
		TriangleType type = triangle.getType(a, b, c);

		// Then
		assertThat(type, is(ERROR));
	}

	@Test
	public void testErrorShortSides() throws Exception {
		// Given
		TriangleInterface triangle = new Triangle();
		int a = 1, b = 1, c = 3;

		// When
		TriangleType type = triangle.getType(a, b, c);

		// Then
		assertThat(type, is(ERROR));
	}

	@Test
	public void testEquilateral() throws Exception {
		// Given
		TriangleInterface triangle = new Triangle();
		int a = 1, b = 1, c = 1;

		// When
		TriangleType type = triangle.getType(a, b, c);

		// Then
		assertThat(type, is(EQUILATERAL));
	}

	@Test
	public void testIsosceles() throws Exception {
		// Given
		TriangleInterface triangle = new Triangle();
		int a = 1, b = 1, c = 2;

		// When
		TriangleType type = triangle.getType(a, b, c);

		// Then
		assertThat(type, is(ISOSCELES));
	}

	@Test
	public void testScalene() throws Exception {
		// Given
		TriangleInterface triangle = new Triangle();
		int a = 1, b = 2, c = 3;

		// When
		TriangleType type = triangle.getType(a, b, c);

		// Then
		assertThat(type, is(SCALENE));
	}

}
